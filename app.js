const express = require('express');
const app = express();
const morgan = require('morgan');
const propertyRoutes = require('./api/routes/properties');
const config = require('dotenv').config()

const allowedClients = (config.parsed.FRONTEND_HOST_URL);

app.use(morgan('dev'));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', allowedClients);
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET');

        return res.status(200).json({});
    }
    next();
});

app.use('/properties', propertyRoutes);

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;