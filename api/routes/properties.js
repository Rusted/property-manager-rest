const express = require('express');
const router = express.Router();
const PropertiesController = require('../controllers/properties')

router.get('/', PropertiesController.list);

module.exports = router;