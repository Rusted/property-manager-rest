var googleMaps = require('@google/maps');
var config = require('dotenv').config();

getGoogleMapsClient = () => {
  return googleMaps.createClient({
    key: config.parsed.GOOGLE_MAPS_API_KEY,
    Promise: Promise
  });
}

getCoordinatesByAddress = (googleMapsClient, address, countryCode) => {
  return new Promise((resolve, reject) => {
      googleMapsClient.geocode({
        address: address,
        region: countryCode
      })
      .asPromise()
      .then((response) => resolve(response.json.results[0].geometry.location))
      .catch(err => reject(err))
    ;
  })
}

isInRadius = (center, radius, coordinates) => {
  rad = (x) => {
    return x * Math.PI / 180;
  };
  
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = rad(center.lat - coordinates.lat);
  var dLong = rad(center.lng - coordinates.lng);
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(coordinates.lat)) * Math.cos(rad(center.lat)) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;

  return d  <= radius;
}

module.exports = {
  getGoogleMapsClient,
  getCoordinatesByAddress,
  isInRadius
}