const map = require('./map');
const config = require('dotenv').config()

updateProperty = (property, location, inArea, fullAddress) => {
    return {...property, location, inArea, fullAddress}
}

getFullAddress = (property) => {
    let fullAddress = '';
    for (var addressPartKey in property.address) {
        if (property.address.hasOwnProperty(addressPartKey)) {
            fullAddress = fullAddress + ' ' + property.address[addressPartKey];
        }
    }

    return fullAddress.trim();
}

module.exports = {updateProperty, getFullAddress};