const propertyStorage = require('../storage/properties');
const propertiesData = require ('../storage/properties.json');
const propertyService = require('../service/property');
const mapService = require('../service/map');

exports.list = (req, res, next) => {
    const googleMapsClient = mapService.getGoogleMapsClient();
    const promises = propertiesData.map((property, key) => {
        const fullAddress = propertyService.getFullAddress(property);
        return mapService.getCoordinatesByAddress(googleMapsClient, fullAddress, 'gb').then((coordinates) => {
            const inArea = mapService.isInRadius({ lat: 51.5073835, lng: -0.1277801 }, 20000, coordinates);
            propertiesData[key] = propertyService.updateProperty(property, coordinates, inArea, fullAddress);
        }).catch((err) => {
            reject(err);
        });
    })

    Promise.all(promises).then(function() {
        res.status(200).json(propertiesData);
    });
}