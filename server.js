const http = require('http');
const app = require('./app');
const config = require('dotenv').config()

const port = process.env.PORT || 3001;
const server = http.createServer(app);
server.listen(port);