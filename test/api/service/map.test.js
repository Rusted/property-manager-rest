var expect = require('chai').expect;

var map = require('../../../api/service/map');

describe('isInRadius', function () {
    let center, radius;
    beforeEach(() => {
        center = { lat: 51.5073835, lng: -0.1277801 };
        radius = 20000;
    });

    it('should be outside radius', function () {
        expect(map.isInRadius(center, radius, {lat: 51.234788, lng: -1.037526})).to.be.equal(false);
    });

    it('should be inside radius', function () {
        expect(map.isInRadius(center, radius, {lat: 51.376597, lng: -0.114554})).to.be.equal(true);
    });
});