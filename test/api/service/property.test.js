var expect = require('chai').expect;
var property = require('../../../api/service/property');

const propertyLondon1 = 
describe('updateProperty', function () {
    let center, propertyLondon1;
    beforeEach(() => {
        center = { lat: 51.5073835, lng: -0.1277801 };
        propertyLondon1 = {
            'owner': 'carlos',
            'address': {
              'line1': 'Flat 5',
              'line4': '7 Westbourne Terrace',
              'postCode': 'W2 3UL',
              'city': 'London',
              'country': 'U.K.'
            },
            'airbnbId': 3512500,
            'numberOfBedrooms': 1,
            'numberOfBathrooms': 1,
            'incomeGenerated': 2000.34,
            'selected': true
          };
    });

    it('should add location, inArea and fullAddress fields to property', function () {
        expect(property.updateProperty(propertyLondon1, center, false, 'Flat 5 and so on'))
        .to.deep.equal({
            ...propertyLondon1,
            location: center,
            inArea: false,
            fullAddress: 'Flat 5 and so on',
        });
    });

    it('should combine all address lines', function () {
        expect(property.getFullAddress(propertyLondon1))
        .to.be.equal(
            'Flat 5 7 Westbourne Terrace W2 3UL London U.K.'
        );
    });
});

describe('getFullAddress', function () {
    let center, propertyLondon1;
    beforeEach(() => {
        center = { lat: 51.5073835, lng: -0.1277801 };
        propertyLondon1 = {
            'owner': 'carlos',
            'address': {
              'line1': 'Flat 5',
              'line4': '7 Westbourne Terrace',
              'postCode': 'W2 3UL',
              'city': 'London',
              'country': 'U.K.'
            }
          };
    });

    it('should combine all address lines', function () {
        expect(property.getFullAddress(propertyLondon1))
        .to.be.equal(
            'Flat 5 7 Westbourne Terrace W2 3UL London U.K.'
        );
    });
});