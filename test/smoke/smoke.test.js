var chai = require('chai').use(require('chai-as-promised'));
var expect = chai.expect;
var request = require('supertest');

var app = require('../../app');

describe('Test properties API Endpoints', function () {
  describe('property manager api', function() {
    it('should get property list successfully', function(done) {
      request(app)
        .get('/properties')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res){
          expect(err).to.equal(null);
          done();
        });
    });

    it('should return 404', function(done) {
      request(app)
        .get('/admin')
        .query({})
        .expect('Content-Type', /json/)
        .expect(404, done);
    });
  });
});