# Description #
Property manager - Node.js project, uses Google Maps API
# Requirements #
Latest npm version
# Installation #
1. ``cp .env.default .env``
2. enter your google maps api key inside .env file
3. ``npm install``
# How to run #
``npm start``
# How to run tests #
``npm test``
# Front-end service #
https://gitlab.com/Rusted/property-manager-client
# Author #
Daumantas Urbanavičius
